/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import SiteUpdateComponent from '@/entities/site/site-update.vue';
import SiteClass from '@/entities/site/site-update.component';
import SiteService from '@/entities/site/site.service';

import LocationService from '@/entities/location/location.service';

import TagService from '@/entities/tag/tag.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Site Management Update Component', () => {
    let wrapper: Wrapper<SiteClass>;
    let comp: SiteClass;
    let siteServiceStub: SinonStubbedInstance<SiteService>;

    beforeEach(() => {
      siteServiceStub = sinon.createStubInstance<SiteService>(SiteService);

      wrapper = shallowMount<SiteClass>(SiteUpdateComponent, {
        store,
        localVue,
        router,
        provide: {
          siteService: () => siteServiceStub,

          locationService: () => new LocationService(),

          tagService: () => new TagService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.site = entity;
        siteServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(siteServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.site = entity;
        siteServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(siteServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundSite = { id: 123 };
        siteServiceStub.find.resolves(foundSite);
        siteServiceStub.retrieve.resolves([foundSite]);

        // WHEN
        comp.beforeRouteEnter({ params: { siteId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.site).toBe(foundSite);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
