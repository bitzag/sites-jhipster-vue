/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import SiteDetailComponent from '@/entities/site/site-details.vue';
import SiteClass from '@/entities/site/site-details.component';
import SiteService from '@/entities/site/site.service';
import router from '@/router';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Site Management Detail Component', () => {
    let wrapper: Wrapper<SiteClass>;
    let comp: SiteClass;
    let siteServiceStub: SinonStubbedInstance<SiteService>;

    beforeEach(() => {
      siteServiceStub = sinon.createStubInstance<SiteService>(SiteService);

      wrapper = shallowMount<SiteClass>(SiteDetailComponent, { store, localVue, router, provide: { siteService: () => siteServiceStub } });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundSite = { id: 123 };
        siteServiceStub.find.resolves(foundSite);

        // WHEN
        comp.retrieveSite(123);
        await comp.$nextTick();

        // THEN
        expect(comp.site).toBe(foundSite);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundSite = { id: 123 };
        siteServiceStub.find.resolves(foundSite);

        // WHEN
        comp.beforeRouteEnter({ params: { siteId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.site).toBe(foundSite);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
