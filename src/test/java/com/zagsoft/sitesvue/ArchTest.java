package com.zagsoft.sitesvue;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.zagsoft.sitesvue");

        noClasses()
            .that()
            .resideInAnyPackage("com.zagsoft.sitesvue.service..")
            .or()
            .resideInAnyPackage("com.zagsoft.sitesvue.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.zagsoft.sitesvue.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
