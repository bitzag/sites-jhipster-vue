import { Component, Vue, Inject } from 'vue-property-decorator';

import { ISite } from '@/shared/model/site.model';
import SiteService from './site.service';

@Component
export default class SiteDetails extends Vue {
  @Inject('siteService') private siteService: () => SiteService;
  public site: ISite = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.siteId) {
        vm.retrieveSite(to.params.siteId);
      }
    });
  }

  public retrieveSite(siteId) {
    this.siteService()
      .find(siteId)
      .then(res => {
        this.site = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
