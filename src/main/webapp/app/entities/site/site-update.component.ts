import { Component, Vue, Inject } from 'vue-property-decorator';

import { required, maxLength } from 'vuelidate/lib/validators';

import LocationService from '@/entities/location/location.service';
import { ILocation } from '@/shared/model/location.model';

import TagService from '@/entities/tag/tag.service';
import { ITag } from '@/shared/model/tag.model';

import { ISite, Site } from '@/shared/model/site.model';
import SiteService from './site.service';

const validations: any = {
  site: {
    name: {
      required,
      maxLength: maxLength(200),
    },
    app: {},
  },
};

@Component({
  validations,
})
export default class SiteUpdate extends Vue {
  @Inject('siteService') private siteService: () => SiteService;
  public site: ISite = new Site();

  @Inject('locationService') private locationService: () => LocationService;

  public locations: ILocation[] = [];

  @Inject('tagService') private tagService: () => TagService;

  public tags: ITag[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.siteId) {
        vm.retrieveSite(to.params.siteId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.site.tags = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.site.id) {
      this.siteService()
        .update(this.site)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Site is updated with identifier ' + param.id;
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.siteService()
        .create(this.site)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = 'A Site is created with identifier ' + param.id;
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveSite(siteId): void {
    this.siteService()
      .find(siteId)
      .then(res => {
        this.site = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.locationService()
      .retrieve()
      .then(res => {
        this.locations = res.data;
      });
    this.tagService()
      .retrieve()
      .then(res => {
        this.tags = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
