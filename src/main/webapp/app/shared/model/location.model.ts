import { ISite } from '@/shared/model/site.model';

import { Environment } from '@/shared/model/enumerations/environment.model';
export interface ILocation {
  id?: number;
  url?: string;
  env?: Environment;
  site?: ISite | null;
}

export class Location implements ILocation {
  constructor(public id?: number, public url?: string, public env?: Environment, public site?: ISite | null) {}
}
