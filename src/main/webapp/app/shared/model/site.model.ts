import { ILocation } from '@/shared/model/location.model';
import { ITag } from '@/shared/model/tag.model';

import { Application } from '@/shared/model/enumerations/application.model';
export interface ISite {
  id?: number;
  name?: string;
  app?: Application | null;
  locations?: ILocation[] | null;
  tags?: ITag[] | null;
}

export class Site implements ISite {
  constructor(
    public id?: number,
    public name?: string,
    public app?: Application | null,
    public locations?: ILocation[] | null,
    public tags?: ITag[] | null
  ) {}
}
