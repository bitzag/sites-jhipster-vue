import { ISite } from '@/shared/model/site.model';

export interface ITag {
  id?: number;
  value?: string;
  sites?: ISite[] | null;
}

export class Tag implements ITag {
  constructor(public id?: number, public value?: string, public sites?: ISite[] | null) {}
}
