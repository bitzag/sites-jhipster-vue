import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const Site = () => import('@/entities/site/site.vue');
// prettier-ignore
const SiteUpdate = () => import('@/entities/site/site-update.vue');
// prettier-ignore
const SiteDetails = () => import('@/entities/site/site-details.vue');
// prettier-ignore
const Location = () => import('@/entities/location/location.vue');
// prettier-ignore
const LocationUpdate = () => import('@/entities/location/location-update.vue');
// prettier-ignore
const LocationDetails = () => import('@/entities/location/location-details.vue');
// prettier-ignore
const Tag = () => import('@/entities/tag/tag.vue');
// prettier-ignore
const TagUpdate = () => import('@/entities/tag/tag-update.vue');
// prettier-ignore
const TagDetails = () => import('@/entities/tag/tag-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default [
  {
    path: '/site',
    name: 'Site',
    component: Site,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/site/new',
    name: 'SiteCreate',
    component: SiteUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/site/:siteId/edit',
    name: 'SiteEdit',
    component: SiteUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/site/:siteId/view',
    name: 'SiteView',
    component: SiteDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/location',
    name: 'Location',
    component: Location,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/location/new',
    name: 'LocationCreate',
    component: LocationUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/location/:locationId/edit',
    name: 'LocationEdit',
    component: LocationUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/location/:locationId/view',
    name: 'LocationView',
    component: LocationDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tag',
    name: 'Tag',
    component: Tag,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tag/new',
    name: 'TagCreate',
    component: TagUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tag/:tagId/edit',
    name: 'TagEdit',
    component: TagUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/tag/:tagId/view',
    name: 'TagView',
    component: TagDetails,
    meta: { authorities: [Authority.USER] },
  },
  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];
