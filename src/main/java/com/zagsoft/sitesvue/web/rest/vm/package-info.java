/**
 * View Models used by Spring MVC REST controllers.
 */
package com.zagsoft.sitesvue.web.rest.vm;
