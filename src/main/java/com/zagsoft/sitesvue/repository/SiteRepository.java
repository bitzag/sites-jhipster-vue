package com.zagsoft.sitesvue.repository;

import com.zagsoft.sitesvue.domain.Site;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Site entity.
 */
@Repository
public interface SiteRepository extends JpaRepository<Site, Long> {
    @Query(
        value = "select distinct site from Site site left join fetch site.tags",
        countQuery = "select count(distinct site) from Site site"
    )
    Page<Site> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct site from Site site left join fetch site.tags")
    List<Site> findAllWithEagerRelationships();

    @Query("select site from Site site left join fetch site.tags where site.id =:id")
    Optional<Site> findOneWithEagerRelationships(@Param("id") Long id);
}
