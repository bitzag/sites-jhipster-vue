package com.zagsoft.sitesvue.domain.enumeration;

/**
 * The Application enumeration.
 */
public enum Application {
    COMMERCE,
    SOURCING,
    PAY_APP,
    CAMPAIGN_MANAGER,
    EQ_COMMERCE,
    Q_NET,
    KS_COMMERCE,
    INTERNAL,
}
