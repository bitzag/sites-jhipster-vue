package com.zagsoft.sitesvue.domain.enumeration;

/**
 * The Environment enumeration.
 */
public enum Environment {
    DEVELOPMENT,
    STAGING,
    PRODUCTION,
    INTERNAL,
}
