package com.zagsoft.sitesvue.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zagsoft.sitesvue.domain.enumeration.Environment;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Location.
 */
@Entity
@Table(name = "location")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Location extends AbstractDateAudit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "url", nullable = false, unique = true)
    private String url;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "env", nullable = false)
    private Environment env;

    @ManyToOne
    @JsonIgnoreProperties(value = { "locations", "tags" }, allowSetters = true)
    private Site site;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Location id(Long id) {
        this.id = id;
        return this;
    }

    public String getUrl() {
        return this.url;
    }

    public Location url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Environment getEnv() {
        return this.env;
    }

    public Location env(Environment env) {
        this.env = env;
        return this;
    }

    public void setEnv(Environment env) {
        this.env = env;
    }

    public Site getSite() {
        return this.site;
    }

    public Location site(Site site) {
        this.setSite(site);
        return this;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Location)) {
            return false;
        }
        return id != null && id.equals(((Location) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Location{" +
            "id=" + getId() +
            ", url='" + getUrl() + "'" +
            ", env='" + getEnv() + "'" +
            "}";
    }
}
