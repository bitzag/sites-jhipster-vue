package com.zagsoft.sitesvue.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zagsoft.sitesvue.domain.enumeration.Application;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Site.
 */
@Entity
@Table(name = "site")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Site extends AbstractDateAudit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 200)
    @Column(name = "name", length = 200, nullable = false, unique = true)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "app")
    private Application app;

    @OneToMany(mappedBy = "site")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "site" }, allowSetters = true)
    private Set<Location> locations = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "rel_site__tag", joinColumns = @JoinColumn(name = "site_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
    @JsonIgnoreProperties(value = { "sites" }, allowSetters = true)
    private Set<Tag> tags = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Site id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Site name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Application getApp() {
        return this.app;
    }

    public Site app(Application app) {
        this.app = app;
        return this;
    }

    public void setApp(Application app) {
        this.app = app;
    }

    public Set<Location> getLocations() {
        return this.locations;
    }

    public Site locations(Set<Location> locations) {
        this.setLocations(locations);
        return this;
    }

    public Site addLocation(Location location) {
        this.locations.add(location);
        location.setSite(this);
        return this;
    }

    public Site removeLocation(Location location) {
        this.locations.remove(location);
        location.setSite(null);
        return this;
    }

    public void setLocations(Set<Location> locations) {
        if (this.locations != null) {
            this.locations.forEach(i -> i.setSite(null));
        }
        if (locations != null) {
            locations.forEach(i -> i.setSite(this));
        }
        this.locations = locations;
    }

    public Set<Tag> getTags() {
        return this.tags;
    }

    public Site tags(Set<Tag> tags) {
        this.setTags(tags);
        return this;
    }

    public Site addTag(Tag tag) {
        this.tags.add(tag);
        tag.getSites().add(this);
        return this;
    }

    public Site removeTag(Tag tag) {
        this.tags.remove(tag);
        tag.getSites().remove(this);
        return this;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Site)) {
            return false;
        }
        return id != null && id.equals(((Site) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Site{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", app='" + getApp() + "'" +
            "}";
    }
}
