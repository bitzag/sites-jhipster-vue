package com.zagsoft.sitesvue.service.mapper;

import com.zagsoft.sitesvue.domain.*;
import com.zagsoft.sitesvue.service.dto.SiteDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Site} and its DTO {@link SiteDTO}.
 */
@Mapper(componentModel = "spring", uses = { TagMapper.class })
public interface SiteMapper extends EntityMapper<SiteDTO, Site> {
    @Mapping(target = "tags", source = "tags", qualifiedByName = "valueSet")
    SiteDTO toDto(Site s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    SiteDTO toDtoId(Site site);

    @Mapping(target = "removeTag", ignore = true)
    Site toEntity(SiteDTO siteDTO);
}
