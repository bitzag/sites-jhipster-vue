package com.zagsoft.sitesvue.service.mapper;

import com.zagsoft.sitesvue.domain.*;
import com.zagsoft.sitesvue.service.dto.LocationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Location} and its DTO {@link LocationDTO}.
 */
@Mapper(componentModel = "spring", uses = { SiteMapper.class })
public interface LocationMapper extends EntityMapper<LocationDTO, Location> {
    @Mapping(target = "site", source = "site", qualifiedByName = "id")
    LocationDTO toDto(Location s);
}
