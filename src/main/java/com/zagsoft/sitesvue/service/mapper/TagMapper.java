package com.zagsoft.sitesvue.service.mapper;

import com.zagsoft.sitesvue.domain.*;
import com.zagsoft.sitesvue.service.dto.TagDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Tag} and its DTO {@link TagDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TagMapper extends EntityMapper<TagDTO, Tag> {
    @Named("valueSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "value", source = "value")
    Set<TagDTO> toDtoValueSet(Set<Tag> tag);
}
